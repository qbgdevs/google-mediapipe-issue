export const initialFrame = {
    frame: undefined,
};

function updateFrame(state, frame) {
    state.frame = frame
}

export function videoReducer(state, action) {
    switch (action.type) {
        case 'reset':
            return initialFrame
        case 'frame':
            return updateFrame(state, action.payload)
        default: return state
    }
}
