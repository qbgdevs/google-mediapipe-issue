import { createContext } from 'react';

export const StateCameraContext = createContext();
export const DispatchCameraContext = createContext();
