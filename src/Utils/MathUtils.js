import * as THREE from 'three';

export const normalizeLandmarks = (landmarks) => {
    let max = {
        x: -Infinity,
        y: -Infinity,
        z: -Infinity,
    };
    let min = {
        x: +Infinity,
        y: +Infinity,
        z: +Infinity,
    };

    landmarks.forEach((point) => {
        if (point.x > max.x) max.x = point.x;
        if (point.y > max.y) max.y = point.y;
        if (point.z > max.z) max.z = point.z;

        if (point.x < min.x) min.x = point.x;
        if (point.y < min.y) min.y = point.y;
        if (point.z < min.z) min.z = point.z;
    });
    landmarks = landmarks.map((point /*, index*/) => {
        return {
            x: (point.x - min.x) / (max.x - min.x),
            y: (point.y - min.y) / (max.y - min.y),
            z: (point.z - min.z) / (max.z - min.z),
        };
    });
    return landmarks;
};



export const rotatePointAroundPivot = (point, pivotPoint, quaternion) => {
    return point.clone().sub(pivotPoint).applyQuaternion(quaternion).add(pivotPoint);
};

export const lookAt = (from, to) => {
    const h = from.clone().add(to).normalize();
    return new THREE.Quaternion(
        from.y * h.z - from.z * h.y,
        from.z * h.x - from.x * h.z,
        from.x * h.y - from.y * h.x,
        from.clone().dot(h)
    );
};

export const landmarkToVector = (landmark) => {
    return new THREE.Vector3(landmark.x, landmark.y, landmark.z);
};

