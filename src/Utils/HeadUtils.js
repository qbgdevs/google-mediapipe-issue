import * as THREE from 'three';
import * as mathUtils from './MathUtils';

export const getNoseTriangle = (landmarks) => {
    let buffer = {};
    buffer.a = mathUtils.landmarkToVector(landmarks[133]);
    buffer.b = mathUtils.landmarkToVector(landmarks[362]);
    buffer.c = mathUtils.landmarkToVector(landmarks[197]);
    let triangle = new THREE.Triangle(buffer.a, buffer.b, buffer.c);
    return triangle;
};

export const getHeadTriangle = (landmarks) => {
    let buffer = {};
    buffer.a = mathUtils.landmarkToVector(landmarks[21]);
    buffer.b = mathUtils.landmarkToVector(landmarks[251]); 
    buffer.c = new THREE.Vector3(
        (landmarks[172].x + landmarks[397].x) / 2,
        (landmarks[172].y + landmarks[397].y) / 2,
        (landmarks[172].z + landmarks[397].z) / 2
    );
    let triangle = new THREE.Triangle(buffer.a, buffer.b, buffer.c);
    return triangle;
};

export const getAbsoluteHeadRotation = (landmarks) => {
    let headTriangle = getHeadTriangle(landmarks);
    let headNormal = new THREE.Vector3();
    headTriangle.getNormal(headNormal);
  
    const headHorizontalAngleRadians = Math.atan(
        (landmarks[362].y - landmarks[133].y) / (landmarks[362].x - landmarks[133].x)
    );
  
    const horizontalRotation = new THREE.Quaternion().setFromAxisAngle ( new THREE.Vector3(0, 0, 1),  headHorizontalAngleRadians) 

    const from = new THREE.Vector3(0, 0, 1);
    const result = mathUtils.lookAt(from, headNormal);

    result.multiply(horizontalRotation).normalize();

    return result;
};

export const getHeadRotation = (landmarks, calibration) => {
    const absHeadRotation = getAbsoluteHeadRotation(landmarks);
    if (calibration.initial_rotation === undefined) {
        calibration.initial_rotation = getAbsoluteHeadRotation(calibration.raw);
    }
    return absHeadRotation.multiply(calibration.initial_rotation.clone().invert());
};
