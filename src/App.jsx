import Main from './Main';
import { useImmerReducer } from 'use-immer';
import { videoReducer, initialFrame } from './Context/Reducer';
import { DispatchCameraContext, StateCameraContext } from './Context/Context';

function App() {
    const [state, dispatch] = useImmerReducer(videoReducer, initialFrame);

    return (
        <div className='App'>
            <DispatchCameraContext.Provider value={dispatch}>
                <StateCameraContext.Provider value={state}>
                    <Main />
                </StateCameraContext.Provider>
            </DispatchCameraContext.Provider>
        </div>
    );
}

export default App;
