import Camera from './Components/Camera/Camera';
import { useRef, useEffect, useState, useCallback } from 'react';
import { FaceMesh } from '@mediapipe/face_mesh';
import Renderer from './Components/Renderer/Renderer';
import { handleFaces } from './Components/Renderer/handleFaces';
import * as headUtils from './Utils/HeadUtils';

function Main() {
    const [renderer, setRenderer] = useState();
    const [camera, setCamera] = useState();
    const [scene, setScene] = useState();
    const [orbitControls, setOrbitControls] = useState();
    const [sceneObjs, setSceneObjs] = useState({});
    const [faceMesh, setFaceMesh] = useState();
    const [canvasCtx, setCanvasCtx] = useState();
    const [fps, setFps] = useState(0);
    const canvasElement = useRef();
    const [calibration, setCalibration] = useState([]);
    const [pivotSliderX, setPivotSliderX] = useState(0.5);
    const [pivotSliderY, setPivotSliderY] = useState(0.3);
    const [pivotSliderZ, setPivotSliderZ] = useState(0.67);

    useEffect(() => {
        setFaceMesh(
            new FaceMesh({
                locateFile: (file) => {
                    return `https://cdn.jsdelivr.net/npm/@mediapipe/face_mesh/${file}`;
                },
            })
        );
    }, [setFaceMesh]);

    useEffect(() => {
        setCanvasCtx(canvasElement.current.getContext('2d'));
    }, []);

    const handleResults = useCallback(
        (results) => {
            if (calibration === undefined) {
                return;
            }
            handleFaces(
                results,
                calibration,
                camera,
                canvasCtx,
                renderer,
                scene,
                sceneObjs,
                canvasElement,
                {
                    x: pivotSliderX,
                    y: pivotSliderY,
                    z: pivotSliderZ,
                }
            );
        },
        [
            calibration,
            camera,
            canvasCtx,
            orbitControls,
            pivotSliderX,
            pivotSliderY,
            pivotSliderZ,
            renderer,
            scene,
            sceneObjs,
        ]
    );
    const handleCalibration = useCallback(
        (results) => {
            let finalCalibration = { raw: [], normalized: [], initial_rotation: undefined };
            finalCalibration.normalized = results.multiFaceLandmarks[0]
            finalCalibration.raw = results.multiFaceLandmarks[0]
            let max = {
                x: -Infinity,
                y: -Infinity,
                z: -Infinity,
            };
            let min = {
                x: +Infinity,
                y: +Infinity,
                z: +Infinity,
            };
            finalCalibration.normalized = finalCalibration.normalized.map((point) => {
                const calibrationPoint = {
                    x: point.x ,
                    y: point.y ,
                    z: point.z ,
                };
                if (calibrationPoint.x > max.x) max.x = calibrationPoint.x;
                if (calibrationPoint.y > max.y) max.y = calibrationPoint.y;
                if (calibrationPoint.z > max.z) max.z = calibrationPoint.z;
        
                if (calibrationPoint.x < min.x) min.x = calibrationPoint.x;
                if (calibrationPoint.y < min.y) min.y = calibrationPoint.y;
                if (calibrationPoint.z < min.z) min.z = calibrationPoint.z;
        
                return calibrationPoint;
            });
            finalCalibration.raw = finalCalibration.raw.map((point) => {
                return {
                    x: point.x ,
                    y: point.y ,
                    z: point.z ,
                };
            });

            finalCalibration.normalized = finalCalibration.normalized.map((point) => {
                return {
                    x: (point.x - min.x) / (max.x - min.x),
                    y: (point.y - min.y) / (max.y - min.y),
                    z: (point.z - min.z) / (max.z - min.z),
                };
            });

            finalCalibration.raw = finalCalibration.raw.map((point) => {
                return {
                    x: point.x - min.x,
                    y: point.y - min.y,
                    z: point.z - min.z,
                };
            });
            let rotQuaternion = headUtils.getAbsoluteHeadRotation(finalCalibration.raw);
            finalCalibration.initial_rotation = rotQuaternion;
            
            setCalibration(finalCalibration);
            

        },
        [canvasCtx]
    );
    useEffect(() => {
        if (
            faceMesh === undefined ||
            calibration === undefined ||
            calibration.raw === undefined ||
            calibration.raw.length <= 0
        ) {
            return;
        }
        faceMesh.onResults(handleResults);
    }, [calibration, faceMesh, handleResults]);

    useEffect(() => {
        if (faceMesh === undefined) {
            return;
        }
        faceMesh.setOptions({
            maxNumFaces: 1,
            refineLandmarks: true,
            minDetectionConfidence: 0.5,
            minTrackingConfidence: 0.5,
        });
        faceMesh.onResults(undefined);
    }, [faceMesh]);

    const startCalibration = () => {
        faceMesh.onResults(handleCalibration);
    };

    return (
        <div>
            <Camera setFps={setFps} faceMesh={faceMesh} />
            <Renderer
                setScene={setScene}
                setCamera={setCamera}
                setRenderer={setRenderer}
                setOrbitControls={setOrbitControls}
                setSceneObjs={setSceneObjs}
                canvasElement={canvasElement}
            ></Renderer>
            {(!calibration || !calibration.raw || calibration.raw.length <= 0) && (
                <button onClick={startCalibration}>Avvia Calibrazione</button>
            )}
            <span>{Math.round(fps)}FPS</span>
            <input
                type='range'
                min='0'
                max='100'
                onChange={(e) => {
                    setPivotSliderX(e.target.value / 100.0);
                }}
                step='1'
            />
            <input
                type='range'
                min='-100'
                max='100'
                onChange={(e) => {
                    setPivotSliderY(e.target.value / 100.0);
                }}
                step='1'
            />
            <input
                type='range'
                min='-400'
                max='100'
                onChange={(e) => {
                    setPivotSliderZ(e.target.value / 100.0);
                }}
                step='1'
            />
        </div>
    );
}

export default Main;
