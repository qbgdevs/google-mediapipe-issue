import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { useEffect } from 'react';

const get3DAxis = () => {
    const tmp = new THREE.Group();
    tmp.add(new THREE.ArrowHelper(new THREE.Vector3(1, 0, 0), new THREE.Vector3(0, 0, 0), 2, 0xff0000));

    tmp.add(new THREE.ArrowHelper(new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 0, 0), 2, 0x00ff00));

    tmp.add(new THREE.ArrowHelper(new THREE.Vector3(0, 0, 1), new THREE.Vector3(0, 0, 0), 2, 0x0000ff));
    return tmp;
};
function Renderer({ setScene, setCamera, setRenderer, setOrbitControls, setSceneObjs, canvasElement }) {
    useEffect(() => {
        const scene = new THREE.Scene();
        const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        const renderer = new THREE.WebGLRenderer();
        renderer.setSize(window.innerWidth / 2, window.innerHeight / 2);
        let sceneObjs = {
            face: {
                points: [],
                eyes: {},
            },
            calibration: {},
        };
        for (let i = 0; i < 478; i++) {
            const sphere = new THREE.Mesh(
                new THREE.SphereGeometry(0.005, 50, 50),
                new THREE.MeshBasicMaterial({
                    color: 0xffffff,
                })
            );
            sceneObjs.face.points.push(sphere);
            scene.add(sphere);
        }

        const calibrationMaterial = new THREE.PointsMaterial({ color: 0xff0000, size: 0.01 });
        let calibrationPoints = [];
        for (let i = 0; i < 478; i++) {
            calibrationPoints.push(new THREE.Vector3(0, 0, 0));
        }
        let calibrationGeometry = new THREE.BufferGeometry();
        calibrationGeometry.setFromPoints(calibrationPoints);
        let calibrationPositions = new Float32Array(calibrationPoints.length * 3); // 3 vertices per point
        calibrationGeometry.setAttribute('position', new THREE.BufferAttribute(calibrationPositions, 3));
        calibrationGeometry.dynamic = true;
        sceneObjs.face.calibration = new THREE.Points(calibrationGeometry, calibrationMaterial);

        const thirdFaceMaterial = new THREE.PointsMaterial({ color: 0x00ff00, size: 0.1 });
        let thirdFacePoints = [];
        for (let i = 0; i < 478; i++) {
            thirdFacePoints.push(new THREE.Vector3(0, 0, 0));
        }
        let thirdFaceGeometry = new THREE.BufferGeometry();
        thirdFaceGeometry.setFromPoints(thirdFacePoints);
        let thirdFacePositions = new Float32Array(thirdFacePoints.length * 3); // 3 vertices per point
        thirdFaceGeometry.setAttribute('position', new THREE.BufferAttribute(thirdFacePositions, 3));
        thirdFaceGeometry.dynamic = true;
        sceneObjs.face.thirdFace = new THREE.Points(thirdFaceGeometry, thirdFaceMaterial);

        scene.add(sceneObjs.face.calibration);
       
        sceneObjs.headRotationAxis = get3DAxis();
        scene.add(sceneObjs.headRotationAxis);

        sceneObjs.debug = {};
        sceneObjs.debug.rotationHelper = get3DAxis();
        scene.add(sceneObjs.debug.rotationHelper);

        sceneObjs.debug.points = [];
        sceneObjs.debug.points[0] = new THREE.Mesh(
            new THREE.SphereGeometry(0.15, 50, 50),
            new THREE.MeshBasicMaterial({
                color: 0xff0000,
            })
        );

        sceneObjs.debug.points[1] = new THREE.Mesh(
            new THREE.SphereGeometry(0.15, 50, 50),
            new THREE.MeshBasicMaterial({
                color: 0x00ff00,
            })
        );

        sceneObjs.debug.points[2] = new THREE.Mesh(
            new THREE.SphereGeometry(0.15, 50, 50),
            new THREE.MeshBasicMaterial({
                color: 0x0000ff,
            })
        );

        scene.add(sceneObjs.debug.points[0]);


        camera.position.z = -5;
        camera.up.set(0, -1, 0);

        setScene(scene);
        setCamera(camera);
        setRenderer(renderer);
        document.body.appendChild(renderer.domElement);
        const controls = new OrbitControls(camera, renderer.domElement);
        setOrbitControls(controls);
        setSceneObjs(sceneObjs);
    }, [setSceneObjs, setCamera, setRenderer, setScene, setOrbitControls]);

    return <canvas ref={canvasElement} id='canvas' className='output_canvas'></canvas>;
}

export default Renderer;
