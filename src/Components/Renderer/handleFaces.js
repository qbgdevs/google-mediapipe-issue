import * as THREE from 'three';
import * as drawingUtils from '@mediapipe/drawing_utils';
import {
    FACEMESH_TESSELATION,
    FACEMESH_RIGHT_EYE,
    FACEMESH_LEFT_EYE,
    FACEMESH_RIGHT_EYEBROW,
    FACEMESH_LEFT_EYEBROW,
    FACEMESH_FACE_OVAL,
    FACEMESH_LIPS,
    FACEMESH_LEFT_IRIS,
    FACEMESH_RIGHT_IRIS,
} from '@mediapipe/face_mesh';

import * as headUtils from '../../Utils/HeadUtils';
import * as mathUtils from '../../Utils/MathUtils';

const drawMesh = (landmarks, canvasCtx) => {
    drawingUtils.drawConnectors(canvasCtx, landmarks, FACEMESH_TESSELATION, {
        color: '#C0C0C070',
        pointsWidth: 1,
    });
    drawingUtils.drawConnectors(canvasCtx, landmarks, FACEMESH_RIGHT_EYE, { color: '#FF3030' });
    drawingUtils.drawConnectors(canvasCtx, landmarks, FACEMESH_RIGHT_EYEBROW, { color: '#FF3030' });
    drawingUtils.drawConnectors(canvasCtx, landmarks, FACEMESH_RIGHT_IRIS, { color: '#FF3030' });
    drawingUtils.drawConnectors(canvasCtx, landmarks, FACEMESH_LEFT_EYE, { color: '#30FF30' });
    drawingUtils.drawConnectors(canvasCtx, landmarks, FACEMESH_LEFT_EYEBROW, { color: '#30FF30' });
    drawingUtils.drawConnectors(canvasCtx, landmarks, FACEMESH_LEFT_IRIS, { color: '#30FF30' });
    drawingUtils.drawConnectors(canvasCtx, landmarks, FACEMESH_FACE_OVAL, { color: '#E0E0E0' });
    drawingUtils.drawConnectors(canvasCtx, landmarks, FACEMESH_LIPS, { color: '#E0E0E0' });
    drawingUtils.drawConnectors(canvasCtx, landmarks, FACEMESH_RIGHT_EYE, { color: '#FF3030' });
};
const updateScene = (calibration, renderer, camera, scene, sceneObjs) => {
    if (renderer === undefined || camera === undefined || scene === undefined) {
        return;
    }

    let p = [];
    let pointIndex = 0;

    // Draw calibration points
    const calib = mathUtils.normalizeLandmarks(calibration.raw, false);
    p = sceneObjs.face.calibration.geometry.attributes.position.array;
    pointIndex = 0;

    for (let i = 0; i < calib.length; i += 1) {
        let point = calib[i];
        p[pointIndex] = point.x;
        p[pointIndex + 1] = point.y * 1;
        p[pointIndex + 2] = point.z;
        pointIndex += 3;
    }
    sceneObjs.face.calibration.geometry.attributes.position.needsUpdate = true;

    renderer.render(scene, camera);
};

export const handleFaces = (
    results,
    calibration,
    camera,
    canvasCtx,
    renderer,
    scene,
    sceneObjs,
    canvasElement,
    pivotSliders
) => {
    canvasElement.current.width = results.image.width;
    canvasElement.current.height = results.image.height;
    canvasCtx.clearRect(0, 0, canvasElement.current.width, canvasElement.current.height);
    canvasCtx.drawImage(results.image, 0, 0, canvasElement.current.width, canvasElement.current.height);
  
    if (results.multiFaceLandmarks) {
        for (let landmarks of results.multiFaceLandmarks) {
            drawMesh(landmarks, canvasCtx);

            landmarks = mathUtils.normalizeLandmarks(landmarks, false);
            const headRotation = headUtils.getHeadRotation(landmarks, calibration);

            const pivot = new THREE.Vector3(
                pivotSliders.x,
                pivotSliders.y,
                pivotSliders.z
            );

            sceneObjs.face.points.forEach((point, pointIndex) => {
                point.position.set(landmarks[pointIndex].x, landmarks[pointIndex].y, landmarks[pointIndex].z);
                const rotatedPoint = mathUtils.rotatePointAroundPivot(
                    point.position,
                    pivot,
                    headRotation.clone().invert()
                );
                point.position.set(rotatedPoint.x, rotatedPoint.y, rotatedPoint.z);
            });

            let newLms = [];
            for (let i = 0; i < sceneObjs.face.points.length; i += 1) {
                newLms.push({
                    x: sceneObjs.face.points[i].position.x,
                    y: sceneObjs.face.points[i].position.y,
                    z: sceneObjs.face.points[i].position.z,
                });
            }
            newLms = mathUtils.normalizeLandmarks(newLms, false);
            for (let i = 0; i < newLms.length; i += 1) {
                sceneObjs.face.points[i].position.setX(newLms[i].x);
                sceneObjs.face.points[i].position.setY(newLms[i].y);
                sceneObjs.face.points[i].position.setZ(newLms[i].z);
            }
            sceneObjs.debug.points[0].position.set(pivot.x, pivot.y, pivot.z);

            updateScene(calibration, renderer, camera, scene, sceneObjs);
        }
    }
    canvasCtx.restore();
};

