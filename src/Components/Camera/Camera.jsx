import { useEffect, useRef, useContext } from 'react';
import '@mediapipe/face_mesh';

import * as cameraUtils from '@mediapipe/camera_utils';
import { DispatchCameraContext } from '../../Context/Context';

function Camera({ setFps, faceMesh }) {
    const dispatch = useContext(DispatchCameraContext);

    const videoElement = useRef(null);

    useEffect(() => {
        if (faceMesh === undefined) {
            return;
        }
        const webcam = new cameraUtils.Camera(videoElement.current, {
            onFrame: async () => {
                let prevTime = performance.now();
                dispatch({ type: 'frame', payload: videoElement.current });
                await faceMesh.send({ image: videoElement.current });
                setFps(1000 / (performance.now() - prevTime));
            },

            width: 1280,
            height: 720,
        });
        webcam.start();
    }, [faceMesh, dispatch, setFps]);

    return (
        <div>
            <video style={{ display: 'none' }} ref={videoElement} className='input_video'></video>
        </div>
    );
}

export default Camera;
